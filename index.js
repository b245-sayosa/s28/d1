//CRUD Operations
	//CRUD operation is the heart of any backend application
	//mastering the CRUD operations is essential for any developer
	// This helps in building character and increasing exposure to logical statements that will help us manipulate
	//mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.

	// [SECTION] CREATE (Inserting document)
		//since MongoDB deals with object as it's structure for documents, we can easily
	//mongo db shell also uses javascript for its syntax which makes it convenient for us to understand its code

	//Insert One document
		/*
			syntax:

			db.collectionName.insertOne({
				object/document
			})
		*/
	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone:"1234567890",
			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScrip", "Python"],
		department:"none"
	})

	// Insert many

		/*
			db.users.insertMany([{ObjectA},])
		*/

	db.users.insertMany([{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "98127352",
            email: "stephenhawkings@gmail.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Niel",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "1256132421",
            email: "nielarmstrong@gmail.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
]);

	db.users.insertOne({
		lastName: "Doe",
		age: 21,
		contact:{
			phone:"1234567890",
			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScrip", "Python"],
		firstName: "Jane"
	})


	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone:"1234567890",
			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScrip", "Python"],
		gender: "Female"
	})

		db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone:"1234567890",
			phone:"67654321",

			email:"janedoe@gmail.com"
		},
		courses:["CSS", "JavaScrip", "Python"],
		gender: "Female"
	})


	//[SECTION] Find (read)

		/*
			Syntax:
				db.collectionName.find();
				// it will show us all the documents that hasits field sets
				db.collectionName.Find({field: value})

		*/

		db.users.find({age: 76});
		db.users.find({firstName: "jane"});
		db.users.find({firstName: "Jane"});



		db.users.find({lastName: "Armstrong", age: 82})	


		// [SECTION] Updating documents 

		db.users.insertOne({
			firstName: "Test",
			lastName: "Test",
			age: 0,
			contact:{
				phone: "000000",
				email: "test@gmail.com"
			},
			course:[],
			department: "none"
		})


		//updateOne
			/*
				Syntax:
				db.collectionName.updateOne({criteria},{$set: {field value}})
			*/

		db.users.updateOne({
			firstName: "Jane"
		},{
			$set:{
				lastName: "Hawking";
			}
		})


		//update multiple docx

		/*
			db

		*/
	db.users.updateMany({
			firstName: "Jane"
		},{
			$set:{
				lastName: "Wick",
				age: 25
			}
		})

	db.users.updateMany({
			department: "none"
		},{
			$set:{
				department: "HR"
			}
		})
//rplacing old document
	/*
		db.users.replaceOne({criteria}, {document/objectToReplace})
	*/


	db.users.replaceOne({firstName: "Test"}, {
		firstName: "Chris",
		lastName: "Mortel"
	})

//[SECTION] Deleting documents

	db.users.deleteOne({firstName: "Jane"})

db.users.deleteMany({firstName: "Jane"})

//[Section] Advanced Queries
	//embedded - nested object
	//nested field - array
//query on embedded document

db.users.find({contact.phone: "87654321"})
//query with exact elements
db.users.find(courses: ["React", "Laravel", "SASS"])

//query an array without regards to order and elements

db.users.find({courses: {$all:["React"]}})